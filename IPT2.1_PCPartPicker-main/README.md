# IPT 2.1 -- WPF PC Part Picker

***PROJECT STATUS: FIRST YEAR PROJECT***

## The IPT Modules

Students of the IMS (Informatikmittelschule Luzern), eng. (College for Computer Science Lucerne) are different to conventional apprentices by lacking practical and empirical experience, which is highly valued in the IT sphere. The administration sought to solve the evident lack of practice by implementing special subjects under the abbreviation IPT (Integrierter Praxis Teil), eng. (Integrated Practical Part). The students will have their theoretical knowledge (Modules) balanced with empirical and practical knowledge and experience (IPT-Modules) by solving exercises, or, which is commonly the case, by programming their own projects within the scope of the module, both training their ability to lead a  project, while reinforcing their knowledge within them.

### IPT 2.1

**IPT 2.1** is paired with the module **M319**, which focuses on **basic programming** using **C#, .NET**. The Project was developed during the **second semester of the first year**.

## Project description

This Project seeks to rudimentarily "generate" a fitting computer in accordance to the given budget and usecase. It includes a DB connection to an Access database, having WPF as an UI, with a simple Algorithm to pick out the best components.

## Notable remarks
It is not recommended to execute the application. While it is confirmed to work, it is an unpolished product and has not been tested for functionality today.
## Dependencies
Microsoft Access Database Engine 2016 Redistributable
